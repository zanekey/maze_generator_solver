package mazeGenerator;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import maze.Maze;
import maze.Cell;

/**
*  @author Zane Keyan
*	class for generating a maze using the growing tree algorithm
*/

public class GrowingTreeGenerator implements MazeGenerator {

	//Instantiating variables
	public Stack<Cell> stack = new Stack<Cell>();
	public List<Cell> visitedCells = new ArrayList<Cell>();
	private int NUMBER_OF_NEIGHBOURS;
	//TraversalArray for the direction of the maze
	private int traversalArray[] = { Maze.EAST , Maze.NORTH , Maze.WEST , Maze.SOUTH , Maze.SOUTHWEST , Maze.NORTHEAST};

	@Override
	public void generateMaze(Maze maze) {

		NUMBER_OF_NEIGHBOURS = maze.type <= Maze.TUNNEL ? 4 : 6;

		// starting point coordinates
		int column = (maze.sizeC) / 2;
		int row = (maze.sizeR) / 2;
		
		// Instantiating the starting cell
		Cell cellInFocus = maze.map[row][column];

		//Add the new cell
		visitedCells.add(maze.map[row][column]);
		stack.push((cellInFocus));

		Cell neighbourCell;
		Cell currentCell;

		while ( !stack.empty()){
				currentCell = stack.peek();
				neighbourCell = getUnvisitedNeighbourCell(currentCell , maze);

				// if cell has no unvisited neighbours , remove it
				if(neighbourCell == null)  stack.pop();
				else stack.push(neighbourCell);
		}
	}

	// get any unvisted cells , else return null
	private Cell getUnvisitedNeighbourCell(Cell currentCell, Maze maze){
		List<Integer> randomList = new ArrayList<>();
		Map<Integer, Cell> randomUnvisitedNeighbours = new HashMap<>();

		int randomNumbers = 0;
		for(int i = 0; i < NUMBER_OF_NEIGHBOURS; i++){

			//getting the neighbour cell row and column
			int r = currentCell.r + maze.deltaR[traversalArray[i]];
			int c = currentCell.c + maze.deltaC[traversalArray[i]];

			// if the neighbour has not been visited , return it , otherwise return null
			if(isIn(maze , r , c) && ! visitedCells.contains(maze.map[r][c])) {
				randomUnvisitedNeighbours.put(randomNumbers++ , new Cell(r , c));
				randomList.add(i);
			}
		}

		// choose a random a neighbour from the available neighbours
		if( randomUnvisitedNeighbours.size() > 0){
			int randomNum = ThreadLocalRandom.current().nextInt(0, randomUnvisitedNeighbours.size() );

			System.out.println(randomUnvisitedNeighbours.size());
			System.out.println(randomNum);
			int randR = randomUnvisitedNeighbours.get(randomNum).r;
			int randC = randomUnvisitedNeighbours.get(randomNum).c;
			visitedCells.add( maze.map[randR][randC]);

			int wallDirection = randomList.get(randomNum);
			maze.map[currentCell.r][currentCell.c].wall[traversalArray[wallDirection]].present = false;
			return randomUnvisitedNeighbours.get(randomNum);
		}

		return null;
	}

	//IsIn function for cells in normal maze
	private boolean isIn(Maze maze, int r, int c) {
		// if normal or tunnel maze return
		if (maze.type <= 1)return r >= 0 && r < maze.sizeR && c >= 0 && c < maze.sizeC;

		// else if a hex maze
		return r >= 0 && r < maze.sizeR && c >= (r + 1) / 2 && c < maze.sizeC + (r + 1) / 2;
	}
}