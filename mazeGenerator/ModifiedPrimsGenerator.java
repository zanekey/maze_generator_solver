package mazeGenerator;

import java.util.*;
import java.util.Random;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import maze.Maze;
import maze.Cell;

/**
 *  @author Zane Keyan
 *	class for generating a maze using the Modified prims algorithm
 */

public class ModifiedPrimsGenerator implements MazeGenerator {

	/*traversalArrays for the direction of the cell*/
	private int traversalArray[] = { Maze.EAST , Maze.NORTH , Maze.WEST , Maze.SOUTH , Maze.SOUTHWEST , Maze.NORTHEAST};

	/*instantiate array variables*/
	private Stack<Cell> frontierCells = new Stack<Cell>();
	private ArrayList<Cell> visitedCells = new ArrayList<Cell>();
	private int NUMBER_OF_NEIGHBOURS;

	@Override
	public void generateMaze(Maze maze) {
		// init
		NUMBER_OF_NEIGHBOURS = maze.type <= Maze.TUNNEL ? 4 : 6;
		
		// starting point coordinates
		int column = (maze.sizeC) / 2;
		int row = (maze.sizeR) / 2;
		
		// Starting cell
		Cell currentCell = maze.map[row][column];
		visitedCells.add(maze.map[row][column]);
		frontierCells = updateFrontierCells(frontierCells , currentCell ,maze);


		// do until there are no more frontier cells
		while(! frontierCells.empty()){
			// get random frontier cell
			int randomNum = ThreadLocalRandom.current().nextInt(0, frontierCells.size() );
			currentCell = frontierCells.get(randomNum);

			// connect frontier cell to a visited cell
			connectFrontierCellToAVisitedNeighbourCell(currentCell , maze);

			// remove the frontier cell from the list of frontier cells
			frontierCells.remove(randomNum);

			// update list of frontier cells
			frontierCells =  updateFrontierCells(frontierCells , currentCell ,maze);
		}

	} // end of generateMaze()

	// connect the selected cell from the frontier cells to a cell that has already been visited
	void connectFrontierCellToAVisitedNeighbourCell(Cell frontierCell  , Maze maze){

		for(int i = 0; i < NUMBER_OF_NEIGHBOURS; i++){

			//getting the neighbour cell row and column
			int r = frontierCell.r + maze.deltaR[traversalArray[i]];
			int c = frontierCell.c + maze.deltaC[traversalArray[i]];

			// if the neigbour cell is already visited , then break the wall and add the frontier cell to the visited list
			if(isIn(maze , r , c) && visitedCells.contains(maze.map[r][c])) {

				maze.map[frontierCell.r][frontierCell.c].wall[traversalArray[i]].present = false;
				visitedCells.add(maze.map[frontierCell.r][frontierCell.c]);
				return;
			}
		}
	}

	// update frontier cells
	private Stack<Cell> updateFrontierCells(Stack<Cell> frontierList , Cell currentCell , Maze maze){
		for(int i = 0; i < NUMBER_OF_NEIGHBOURS; i++){

			//getting the neighbour cell row and column
			int r = currentCell.r + maze.deltaR[traversalArray[i]];
			int c = currentCell.c + maze.deltaC[traversalArray[i]];

			// if the neighbour has not been visited , return it , otherwise return null
			if(isIn(maze , r , c) && ! visitedCells.contains(maze.map[r][c]) && ! frontierList.contains(maze.map[r][c]))
				frontierList.push(maze.map[r][c]);
		}
		return frontierList;
	}

	//IsIn function for cells in normal maze
	protected boolean isIn(Maze maze, int r, int c) {
		// if normal or tunnel maze return
		if (maze.type <= 1)return r >= 0 && r < maze.sizeR && c >= 0 && c < maze.sizeC;
		// else if a hex maze
		return r >= 0 && r < maze.sizeR && c >= (r + 1) / 2 && c < maze.sizeC + (r + 1) / 2;
	}

} // end of class ModifiedPrimsGenerator