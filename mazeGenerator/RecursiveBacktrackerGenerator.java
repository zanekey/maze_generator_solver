package mazeGenerator;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import maze.Maze;
import maze.Cell;
import org.omg.Messaging.SYNC_WITH_TRANSPORT;

/**
 *  @author Zane Keyan
 *	class for generating a maze using the Recursive Backtracker
 */

public class RecursiveBacktrackerGenerator implements MazeGenerator {
	
	/*instantiate variables*/
	private Stack<Cell> backtrackStack = new Stack<Cell>();
	
	/*traversalArray is used to get the direction of the maze*/
	private int traversalArray[] = { Maze.EAST , Maze.NORTH , Maze.WEST , Maze.SOUTH , Maze.SOUTHWEST , Maze.NORTHEAST};
	
	/*List of visited cells*/
	private List<Cell> visitedCells = new ArrayList<Cell>();
	private int NUMBER_OF_NEIGHBOURS;

	@Override
	public void generateMaze(Maze maze) {
		NUMBER_OF_NEIGHBOURS = maze.type <= Maze.TUNNEL ? 4 : 6;
		/* number of cells calculator */
		int numberOfCells = maze.sizeC * maze.sizeR;
		
		/* starting cell coordinates */
		int c = (maze.sizeC) / 2;
		int r = (maze.sizeR) / 2;
		
		/* starting cell */
		Cell currentCell = maze.map[r][c];

		visitedCells.add(maze.map[r][c]);
		backtrackStack.push(maze.map[r][c]);


		while(! backtrackStack.empty()){
			// get an adjacent cell to the neighbouring cell
			currentCell = backtrackStack.peek();
			currentCell = getNeighbourCell(currentCell , maze);

			// if current cell is empty , this means it has no unvisited neighbours, hence backtrack
			if (currentCell == null) backtrackStack.pop();
			else  backtrackStack.push(currentCell);

		}
	}

	private Cell getNeighbourCell(Cell currentCell , Maze maze){

		if(currentCell.tunnelTo != null && !visitedCells.contains(currentCell.tunnelTo)){
			visitedCells.add(currentCell.tunnelTo);
			return  currentCell.tunnelTo;
		}

		List<Integer> randomNeighbourList = new ArrayList<>();

		for(int i = 0; i < NUMBER_OF_NEIGHBOURS; i++){
			//getting the neighbour cell row and column
			int r = currentCell.r + maze.deltaR[traversalArray[i]];
			int c = currentCell.c + maze.deltaC[traversalArray[i]];

			// if the neighbour has not been visited , return it , otherwise return null
			if(isIn(maze , r , c) && ! visitedCells.contains(maze.map[r][c]))
				randomNeighbourList.add(i);
		}

		// choosing a random neighbour makes the maze more complicated
		if (randomNeighbourList.size() > 0){
			int randomNum = ThreadLocalRandom.current().nextInt(0, randomNeighbourList.size() );
			int randomNeighbour = randomNeighbourList.get(randomNum);

			int r = currentCell.r + maze.deltaR[traversalArray[randomNeighbour]];
			int c = currentCell.c + maze.deltaC[traversalArray[randomNeighbour]];

			visitedCells.add( maze.map[r][c]);

			maze.map[currentCell.r][currentCell.c].wall[traversalArray[randomNeighbour]].present = false;
			return maze.map[r][c];
		}

		return  null;
	}

	//IsIn function for cells in normal maze
	protected boolean isIn(Maze maze, int r, int c) {
		// if normal or tunnel maze return
		if (maze.type <= 1)return r >= 0 && r < maze.sizeR && c >= 0 && c < maze.sizeC;

		// else if a hex maze
		return r >= 0 && r < maze.sizeR && c >= (r + 1) / 2 && c < maze.sizeC + (r + 1) / 2;
	}

} // end of class RecursiveBacktrackerGenerator