package mazeSolver;

import java.util.*;
import maze.Maze;
import maze.Cell;

/**
 * @author  Zane Keyan
 * Implements the BiDirectional recursive backtracking maze solving algorithm.
 */
public class BiDirectionalRecursiveBacktrackerSolver implements MazeSolver {

	/*instantiate variables */
	private List<Cell> entranceVisitedCells = new ArrayList<Cell>();
	private List<Cell> exitVisitedCells = new ArrayList<Cell>();
	private Stack<Cell> entranceStack = new Stack<Cell>();
	private Stack<Cell> exitStack = new Stack<Cell>();
	private boolean pathsCollided = false;

	private int traversalArray[];
	private int normalMazeTraversal[] = { Maze.EAST , Maze.SOUTH , Maze.WEST , Maze.NORTH};
	private int hexMazeTraversal[] = { Maze.EAST ,  Maze.SOUTH , Maze.SOUTHWEST , Maze.WEST , Maze.NORTH , Maze.NORTHEAST};
	private int NUMBER_OF_NEIGHBOURS;

	@Override
	public void solveMaze(Maze maze) {

		// init fields
		NUMBER_OF_NEIGHBOURS = maze.type <= Maze.TUNNEL ? 4 : 6;
		traversalArray = maze.type <= Maze.TUNNEL ? normalMazeTraversal : hexMazeTraversal;

		// instantiate the two starting points
		Cell currentEntranceCell = maze.entrance;
		Cell currentExitCell = maze.exit;

		entranceStack.push(currentEntranceCell);
		exitStack.push(currentExitCell);

		Cell nextEntranceCell;
		Cell nextExitCell;

		// do while the two paths starting from the entrance and exit have not met
		while(! pathsCollided){

			// handle entrance path
			currentEntranceCell = entranceStack.peek();
			currentExitCell = exitStack.peek();

			// determine if paths have met
			if(currentEntranceCell == currentExitCell )
				pathsCollided = true;

			// handle entrance path
			nextEntranceCell = iterateToNextCell(currentEntranceCell, maze , entranceVisitedCells);
			if (nextEntranceCell == null) entranceStack.pop();
			else {
				if (!entranceVisitedCells.contains(nextEntranceCell))
					entranceVisitedCells.add(nextEntranceCell);

					entranceStack.push(nextEntranceCell);
				}

			// handle exit path
			nextExitCell = iterateToNextCell(currentExitCell, maze , exitVisitedCells);
			if (nextExitCell == null) exitStack.pop();
			else {
				if (!exitVisitedCells.contains(nextExitCell))
					exitVisitedCells.add(nextExitCell);

					exitStack.push(nextExitCell);
				}
		}

		// draw entrance path
		for(Cell entranceCell : entranceStack)
			maze.drawFtPrt(entranceCell);

		// draw exit path
		for(Cell exitCell: exitStack)
			maze.drawFtPrt(exitCell);

	} // end of solveMaze()s

	private Cell iterateToNextCell(Cell currentCell , Maze maze , List<Cell> visitedCells){

		boolean pathBlocked;
		for(int i = 0 ; i < NUMBER_OF_NEIGHBOURS; i++){

			int r = currentCell.r + maze.deltaR[traversalArray[i]];
			int c = currentCell.c + maze.deltaC[traversalArray[i]];


			if(isIn(maze , r , c) && ! visitedCells.contains(maze.map[r][c]) ){
				pathBlocked = maze.map[currentCell.r][currentCell.c].wall[traversalArray[i]].present;

				if( ! pathBlocked)
					return  maze.map[r][c];

			}
		}
		return null;
	}

	@Override
	public boolean isSolved() {
		// TODO Auto-generated method stub
		return pathsCollided;
	} // end if isSolved()


	@Override
	public int cellsExplored() {
		return entranceStack.size() + exitStack.size();
	} // end of cellsExplored()


	//IsIn function for cells in normal maze
	private boolean isIn(Maze maze, int r, int c) {
		// if normal or tunnel maze return
		if (maze.type <= 1)return r >= 0 && r < maze.sizeR && c >= 0 && c < maze.sizeC;

		// else if a hex maze
		return r >= 0 && r < maze.sizeR && c >= (r + 1) / 2 && c < maze.sizeC + (r + 1) / 2;
	}

} // end of class BiDirectionalRecursiveBackTrackerSolver
