package mazeSolver;

import java.util.*;
import maze.Maze;
import maze.Cell;

/**
 * @author  Zane Keyan
 * Implements the Wall Follower maze solving algorithm.
 */

public class WallFollowerSolver implements MazeSolver {
	
	/* list for vistited arrays */
	private List<Cell> visitedCells = new ArrayList<Cell>();
	private Stack<Cell> backtrackStack = new Stack<Cell>();

	/* traversalArray */private int traversalArray[];
	private int normalMazeTraversal[] = { Maze.EAST , Maze.SOUTH , Maze.WEST , Maze.NORTH};
	private int hexMazeTraversal[] = { Maze.EAST ,  Maze.SOUTH , Maze.SOUTHWEST , Maze.WEST , Maze.NORTH , Maze.NORTHEAST};
	private int NUMBER_OF_NEIGHBOURS;

	@Override
	public void solveMaze(Maze maze) {
		// TODO Auto-generated method stub

		NUMBER_OF_NEIGHBOURS = maze.type <= Maze.TUNNEL ? 4 : 6;
		traversalArray = maze.type <= Maze.TUNNEL ? normalMazeTraversal : hexMazeTraversal;

		/*get starting cell */	
		Cell currentCell = maze.entrance;
		backtrackStack.push(currentCell);
		visitedCells.add(currentCell);

		//Visualisation for the solution
		maze.drawFtPrt(currentCell);
		Cell nextCell;

		// keep iterating until maze exit is reached
		while(currentCell != maze.exit){

			currentCell = backtrackStack.peek();
			nextCell = iterateToNextCell(currentCell , maze);

			if(nextCell == null) backtrackStack.pop();
			else {
				// if cell is not visited before , add it
				if(!visitedCells.contains(nextCell))  visitedCells.add(nextCell);

				// push to stack and draw
				backtrackStack.push(nextCell);
				maze.drawFtPrt(currentCell);
			}
		}

		// draw last cell
		maze.drawFtPrt(currentCell);
	
		
	} // end of solveMaze()

	 // func to get a cell adjacent to the cell parsed in
	 private Cell iterateToNextCell(Cell currentCell , Maze maze){

		// if the cell is connected to a tunnel
		if(currentCell.tunnelTo != null && ! visitedCells.contains(currentCell.tunnelTo))
			return currentCell.tunnelTo;

		// flag if wall is available or nor
		boolean pathBlocked;
		for(int i = 0 ; i < NUMBER_OF_NEIGHBOURS; i++){

			// neighboring r and c values
			int r = currentCell.r + maze.deltaR[traversalArray[i]];
			int c = currentCell.c + maze.deltaC[traversalArray[i]];

			// determine new cell is within maze bounds and has not been visited already
			if(isIn(maze , r , c) && ! visitedCells.contains(maze.map[r][c]) ){
				pathBlocked = maze.map[currentCell.r][currentCell.c].wall[traversalArray[i]].present;

				if( ! pathBlocked)
					return  maze.map[r][c];

			}
		}
		return null;
	 }

	@Override
	public boolean isSolved()
	{
		return true;
	} // end if isSolved()

	@Override
	public int cellsExplored() {
		return visitedCells.size();
	} // end of cellsExplored()

	 //IsIn function for cells in normal maze
	 private boolean isIn(Maze maze, int r, int c) {
		 // if normal or tunnel maze return
		 if (maze.type <= 1)return r >= 0 && r < maze.sizeR && c >= 0 && c < maze.sizeC;

		 // else if a hex maze
		 return r >= 0 && r < maze.sizeR && c >= (r + 1) / 2 && c < maze.sizeC + (r + 1) / 2;
	 }
	


} // end of class WallFollowerSolver