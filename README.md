# Maze Generator / Solver #

Generate and solve 2D, rectangular and hexagon, perfect mazes using various algorithms

###  Background ###

This project was completed as part of a university assignment. The lecturer provided the skeleton code and students were required to implement a number of different algorithms to generate
and solve 2d perfect mazes. The three different types of mazes are as follows:
	* Rectangular Maze
	* Hexagon Maze
	* Rectangular maze with tunnels
	
### Maze Generator Algorithms Implemented ###

* Recursive Backtracker
* Prim's
* Growing Tree

### Maze Solving Algorithms Implemented ###

* Wall Follower
* Bidirectional Recursive Backtracker

![hexagon maze](images/hex_maze.PNG hexagaon_maze)

![tunnel maze](images/tunnel_maze_recur.PNG tunnelmaze)

![tunnel_maze_wallfollower](images/tunnel_maze_wallfollower.PNG wallfollowersolver)


### How do I get set up? ###

*Built using Java 8
*IntelliJ IDE
Compiling and Executing

To compile the les, run the following command from the root directory:

	javac -cp .:mazeSolver/SampleSolver.jar *.java

To run the maze visualisation and evaluation algorithm, run the following command:

	java -cp .:mazeSolver/SampleSolver.jar MazeTester<parameter file><visualise maze and solver>
	
	
Or alternatively you can use one of the parameter files provided in this repo ( see example1.para and example2.para) to test the program.
The program accepts files in the following format.


mazeType
generatorNamesolverName
numRownumCol
entranceRow entranceCol
exitRow exitCol
tunnelList

* mazeType: The type of maze to generate, where it should be one of normal, hex , tunnel.
* generatorName: Name of maze generation algorithm, where it should be one of frecurBack, growingTree, modiPrimg. recurBack is recursive backtracker,
	growingTree is the growing tree algorithm, and modiPrim is (modied) prim�s algorithm.
* solverName: Name of the maze generation algorithm, where it should be one of fwallFollower, biDirrecurBack, sample, noneg. biDirrecurBack is bidirectional Recursive Backtracker, wallFol-lower is the wall follower algorithm (solver), sample is a sample solver for you to visualise solving and none is to specify that no solving is wanted.
* numRow: Number of rows in the generated maze, it should be a positive integer (1 or greater).
* numCol: Number of columns in the generated maze, it should be a positive integer (1 or greater).
* entranceRow: the row of the entrance cell, should be in range [0, numRow-1].
* entranceCol: the column of the entrance cell, should be in range [0, numCol-1].
* exitRow: the row of the exit cell, should be in range [0, numRow-1].
* exitCol: the column of the exit cell, should be in range [0, numCol-1].



### Contribution guidelines ###

* skeleton code
	* @author Youhan Xia
 	* @author Jeffrey Chan
* Algorithms Implementation
	* @author Zane Keyan
